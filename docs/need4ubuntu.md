---------
What you need to install into ubuntu at this project
======================================================
[TOC]


----------


Project vars definition in this doc
-----------------------------------

proj_name `where2stay` also its defined at `src/where2stay/settings.py` in `PROJ_NAME`
site_root `/srv/www/proj_name/`also it's defined at `src/where2stay/settings.py` in `SITE_ROOT`

----------


Python, packages & enviroment
-----------------------------
**install** basic pacages

    sudo apt-get install git-core python-pip python-dev virtualenv

configure project dir & user

    cd /srv
    sudo usermod -a -G www-data mcgr0g 
    sudo chown -R mcgr0g:www-data /srv/
    sudo chmod 2755 /srv/www/

### boost your workflow in venv

**run**

    mkdir /srv/www/.venvs
    sudo pip install virtualenvwrapper

**edit** your *.bashrc*

    export WORKON_HOME=/srv/www/.venvs
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    export VIRTUALENVWRAPPER_VIRTUALENV=/usr/bin/virtualenv
    export PIP_REQUIRE_VIRTUALENV=true
    source /usr/local/bin/virtualenvwrapper.sh

**run**

    mkvirtualenv proj_name
    pip install -r site_root/requirements.txt
    deactivate

>**Note:** *proj_name* & *site_root* you can fine at definition section if this file

----------
DataBase
--------
### MySQL
**install** db server

    sudo apt-get install libmysqlclient-dev, python-mysqldb, mysql-server
    
**run** to configure project base & user

    mysql -uroot -p
    #enter pass
    CREATE DATABASE proj_name CHARACTER SET utf8 COLLATE utf8_general_ci;
    create user 'proj_db_user'@'localhost' IDENTIFIED BY 'userpass';
    GRANT ALL PRIVILEGES ON proj_name.* TO 'proj_db_user'@'localhost';

>**Note:** 
> *proj_name* you can fine at definition section if this file
> *proj_db_user*, *user_db_pass*  your can find in [settings_local.py][3]

----------
Images
------
### Pillow
**install** requirements

    sudo apt-get install libtiff4-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.5-dev tk8.5-dev

**run** to install Pillow

    workon proj_name
    pip install Pillow

>**Note:** *proj_name*  you can fine at definition section if this file
### nginx
**run** to use configs from repo

    ln -s /srv/www/proj_name/conf/proj_name.conf /etc/nginx/sites-available/
    ln -s /etc/nginx/sites-available/proj_name.conf /etc/nginx/sites-available/proj_name.conf
    sudo nginx -s reload
    
>**Note:** proj_name & site_root you can fine at definition section if this file

### uwsgi
**run** to use configs from repo

    ln -s site_root/conf/proj_name.ini /etc/uwsgi/apps/
    touch /site_root/touchme

>**Note:** *proj_name* & *site_root* you can fine at definition section if this file

**NOTE:** You can find more information:
> - try **StackEdit**  [here][1],
> - about **Markdown** syntax [here][2],
  [1]: https://stackedit.io/ "StackEdit"
  [2]: https://bitbucket.org/tutorials/markdowndemo/overview "Markdown demo for BitBucket"
  [3]: https://bitbucket.org/mcgr0g/where2stay/raw/master/docs/local_settings.py


