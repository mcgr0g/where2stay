from settings import *
#my private data
MANAGERS = (
    ('Ronnie McGrog', 'manager@company.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': PROJ_NAME,
        'USER': 'real_user',
        'PASSWORD': 'real_pass',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'user@gmail.com'
EMAIL_HOST_PASSWORD = 'pass'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_SUBJECT_PREFIX = '[' + PROJ_NAME + ']'
