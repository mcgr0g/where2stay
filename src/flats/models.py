# coding=utf-8
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from easy_thumbnails.files import get_thumbnailer
from geopy import geocoders
from django.conf import settings

geo = geocoders.GoogleV3()


class Flat(models.Model):
    name = models.CharField(max_length=200, default='flat_name')
    overview = models.TextField(blank=True)
    published = models.BooleanField(default=False)
    isFree = models.BooleanField("it is boobked", default=True)
    price = models.IntegerField(max_length=5, blank=True, default=150)
    address = models.CharField(max_length=200)
    geo_lat = models.FloatField(default=settings.EASY_MAPS_CENTER[0])
    geo_lng = models.FloatField(default=settings.EASY_MAPS_CENTER[1])

    def __unicode__(self):
        return self.name

    def roomNumber(self):
        return Interior.objects.filter(flat=self.id).count()

    def save(self, *args, **kwargs):
        place, (lat, lng) = geo.geocode(self.address)
        self.geo_lat = lat
        self.geo_lng = lng
        super(Flat, self).save(*args, **kwargs)


def upload_path(self, filename):
    split = filename.rsplit('.', 1)
    extension = split[1]
    if self.id is None:
        inter = self.name
    else:
        inter = self.id
    return 'admin/flat' + str(self.flat.id) + '/interior' + str(inter) + '.' + extension

# return 'admin/flat' + str(self.flat.id) + '/interior-' + str(self.name) + '.' + extension


class Interior(models.Model):
    flat = models.ForeignKey(Flat, null=True)
    name = models.CharField(max_length=200, default='room')
    description = models.TextField(blank=True)
    panorama = models.BooleanField(default=False)
    photo = ThumbnailerImageField(default="", upload_to=upload_path, blank=True)
    thumb_url = models.CharField(max_length=500, default='link/to/thumbnail/')

    def __unicode__(self):
        return self.name

    def image_thumb(self):
        return '<img src="%s" />' % get_thumbnailer(self.photo)['flat_small'].url

    image_thumb.short_description = 'Thumb'
    image_thumb.allow_tags = True

    def save(self, *args, **kwargs):
        self.thumb_url = get_thumbnailer(self.photo)['flat_small'].url
        super(Interior, self).save(*args, **kwargs)