from django.conf.urls import patterns, url

from flats import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
	# ex: /flat/5/
    url(r'^(?P<flat_id>\d+)/$', views.detail, name='detail'),
	url(r'^(?P<flat_id>\d+)/gallery/$', views.bigmonitor, name='gallery'),
	url(r'^(?P<flat_id>\d+)/panorama/(?P<interior_id>\d+)/$', views.panorama, name='panorama'),
)