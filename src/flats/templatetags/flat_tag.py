from django import template
from django.template import Context
from flats.models import Flat

register = template.Library()

def show_flat_list():
	list = Flat.objects.all()
	return {'flats': list}
register.inclusion_tag('flats/flat_list.html')(show_flat_list)

