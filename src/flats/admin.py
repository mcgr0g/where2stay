from django.contrib import admin
from flats.models import Flat, Interior
from django import forms
from easy_maps.widgets import AddressWithMapWidget


class InteriorInline(admin.StackedInline):
	model = Interior
	fields = (("name", "panorama", "photo"), ("description", "image_thumb"))
	readonly_fields = ("image_thumb",)
	extra = 0


class FlatAdmin(admin.ModelAdmin):
	class form(forms.ModelForm):
		class Meta:
			widgets = {
				'address': AddressWithMapWidget({'class': 'vTextField'})
			}

	fields = (('name', 'published', 'isFree'), 'overview', ('address', 'price'),)
	inlines = [InteriorInline, ]
	list_display = ('name', 'isFree', 'published')
	save_on_top = True


admin.site.register(Flat, FlatAdmin)
