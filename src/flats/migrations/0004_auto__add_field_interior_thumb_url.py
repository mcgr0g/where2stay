# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Interior.thumb_url'
        db.add_column(u'flats_interior', 'thumb_url',
                      self.gf('django.db.models.fields.CharField')(default='link/to/thumbnail/', max_length=500),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Interior.thumb_url'
        db.delete_column(u'flats_interior', 'thumb_url')


    models = {
        u'flats.flat': {
            'Meta': {'object_name': 'Flat'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'geo_lat': ('django.db.models.fields.FloatField', [], {'default': '-41.3'}),
            'geo_lng': ('django.db.models.fields.FloatField', [], {'default': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'isFree': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'flat_name'", 'max_length': '200'}),
            'overview': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'price': ('django.db.models.fields.IntegerField', [], {'default': '150', 'max_length': '5', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'flats.interior': {
            'Meta': {'object_name': 'Interior'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['flats.Flat']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'room'", 'max_length': '200'}),
            'panorama': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'thumb_url': ('django.db.models.fields.CharField', [], {'default': "'link/to/thumbnail/'", 'max_length': '500'})
        }
    }

    complete_apps = ['flats']