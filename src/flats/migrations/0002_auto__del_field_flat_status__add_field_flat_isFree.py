# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Flat.status'
        db.delete_column(u'flats_flat', 'status')

        # Adding field 'Flat.isFree'
        db.add_column(u'flats_flat', 'isFree',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Flat.status'
        db.add_column(u'flats_flat', 'status',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)

        # Deleting field 'Flat.isFree'
        db.delete_column(u'flats_flat', 'isFree')


    models = {
        u'flats.flat': {
            'Meta': {'object_name': 'Flat'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'isFree': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'flat_name'", 'max_length': '200'}),
            'overview': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'price': ('django.db.models.fields.IntegerField', [], {'default': '150', 'max_length': '5', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'flats.interior': {
            'Meta': {'object_name': 'Interior'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['flats.Flat']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'room'", 'max_length': '200'}),
            'panorama': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['flats']