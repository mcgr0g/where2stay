# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Flat.geo_lat'
        db.add_column(u'flats_flat', 'geo_lat',
                      self.gf('django.db.models.fields.FloatField')(default=-41.3),
                      keep_default=False)

        # Adding field 'Flat.geo_lng'
        db.add_column(u'flats_flat', 'geo_lng',
                      self.gf('django.db.models.fields.FloatField')(default=32),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Flat.geo_lat'
        db.delete_column(u'flats_flat', 'geo_lat')

        # Deleting field 'Flat.geo_lng'
        db.delete_column(u'flats_flat', 'geo_lng')


    models = {
        u'flats.flat': {
            'Meta': {'object_name': 'Flat'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'geo_lat': ('django.db.models.fields.FloatField', [], {'default': '-41.3'}),
            'geo_lng': ('django.db.models.fields.FloatField', [], {'default': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'isFree': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'flat_name'", 'max_length': '200'}),
            'overview': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'price': ('django.db.models.fields.IntegerField', [], {'default': '150', 'max_length': '5', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'flats.interior': {
            'Meta': {'object_name': 'Interior'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'flat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['flats.Flat']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'room'", 'max_length': '200'}),
            'panorama': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['flats']