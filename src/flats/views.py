from django.http import Http404
from django.template import Context
from django.shortcuts import render, get_object_or_404, get_list_or_404
from flats.models import Flat, Interior


def index(request):
    flat_list = Flat.objects.all()
    context = Context({
        'flat_list': flat_list,
    })
    return render(request, 'flats/flats.html', context)


def detail(request, flat_id):
    # return HttpResponse("You're looking at flat %s." % flat_id)
    flat = get_object_or_404(Flat, pk=flat_id)
    interior = get_list_or_404(Interior, flat=flat_id)
    return render(request, 'flats/detail.html', {'flat': flat, 'interior': interior})


def bigmonitor(request, flat_id):
    flat = get_object_or_404(Flat, pk=flat_id)
    interior = get_list_or_404(Interior, flat=flat_id)
    return render(request, 'flats/gallery.html', {'flat': flat, 'interior': interior})


def panorama(request, flat_id, interior_id):
    flat = get_object_or_404(Flat, pk=flat_id)
    interior = get_list_or_404(Interior, flat=flat_id, id=interior_id)
    int = interior[0]
    if not int.panorama:
        raise Http404
    return render(request, 'flats/panorama.html', {'flat': flat, 'interior': interior[0]})