from django import forms

class ContactForm(forms.Form):
    sender = forms.EmailField(label='Your email')
    subject = forms.CharField(max_length=100, label='Subject')
    message = forms.CharField(label='message')
