from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),

                       # Examples:
                       url(r'^$', 'where2stay.views.home', name='home'),
                       # url(r'^where2stay/', include('where2stay.foo.urls')),
                       # Uncomment the admin/doc line below to enable admin documentation:
                       # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       url(r'^apartments/', include('flats.urls', namespace='flats')),
                       url(r'^contact/$', 'where2stay.views.contact', name="contact"),
)

if settings.DEBUG:
    urlpatterns += patterns('',
                            (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                                'document_root': settings.MEDIA_ROOT
                            })
    )