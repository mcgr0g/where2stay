# Django settings for where2stay project.
from genericpath import isfile
import sys
from socket import gethostname
from os.path import dirname, abspath, join

PROJ_NAME = "where2stay"
PRODUCTION_SERVER = "www.dude.routemehome.com"

# [PROJECT_NAME]=(site_root)
# |-[conf]
# |-[docs]
# |-[logs]
# |-[media]
# |-[pid]
# |-[db]
# |-[src]=(proj_root)
# 	|-[templates]
# 	|-settings.py=(this file)
# 	|-urls.py
# 	|-wsgi.py
# 	|-manage.py
# |-[static]
# |-requirements.txt

SITE_ROOT = dirname(dirname(dirname(abspath(__file__))))
PROJECT_ROOT = dirname(dirname(abspath(__file__)))
CUR_DIR = dirname(abspath(__file__))

conf_root = join(SITE_ROOT, 'conf').replace('\\', '/')
nginx_conf = join(conf_root, PROJ_NAME + '.conf')
hosts = []
if isfile(nginx_conf):
    file = open(nginx_conf, 'r')
    for line in file:
        if line.lstrip().startswith("server_name"):
            line = line.rstrip("\n;")
            hosts = line.split("server_name")[1].split()
        #print "I found hosts from nginx", hosts

DEBUG = gethostname() != PRODUCTION_SERVER
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('admin', 'admin@mcgrog.now.im'),
)

MANAGERS = ADMINS
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': PROJ_NAME,
        'USER': 'proj_db_user',
        'PASSWORD': 'user_db_pass',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}

ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'dev']
for item in hosts:
    ALLOWED_HOSTS.append(item)

# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'Asia/Novosibirsk'

# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = join(SITE_ROOT, 'media/').replace('\\', '/')

MEDIA_URL = '/media/'

STATIC_ROOT = join(SITE_ROOT, 'static/').replace('\\', '/')

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    join(PROJECT_ROOT, 'templates').replace('\\', '/'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '3%z*gab-f)rq9fq#rk#$q#@9*2(el^pmy-^4@c=+%h!p+%!=^b'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'where2stay.urls'

WSGI_APPLICATION = 'where2stay.wsgi.application'

TEMPLATE_DIRS = (
    join(PROJECT_ROOT, 'templates').replace('\\', '/'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    # 'django.contrib.admindocs',
    'south',
    'flats',
    'easy_thumbnails',
    'django_cleanup',
    'easy_maps',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
log_root = join(SITE_ROOT, 'logs').replace('\\', '/')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s|%(asctime)s|%(module)s|%(process)d|%(thread)d|%(message)s',
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s|%(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'logfile': {
            'level': "DEBUG",
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(log_root, 'django_debug.log'),
            'maxBytes': 1024 * 1024 * 5, # 5MB
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

sys.path.insert(0, join(SITE_ROOT, "src"))

THUMBNAIL_ALIASES = {
    '': {
        'flat_small': {'size': (150, 150), 'crop': True},
    },
}

if isfile(join(CUR_DIR, 'settings_local.py')):
    try:
        from settings_local import *
    except Exception, e:
        import os, warnings

        warnings.warn("Unable import local settings [%s]: %s" % (type(e), e))
        sys.exit(1)

if DEBUG:
    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )
    INSTALLED_APPS += ('debug_toolbar', )
    INTERNAL_IPS = ('127.0.0.1', '192.168.1.225', '192.168.1.153')
    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }
    TEMPLATE_LOADERS = (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )

EASY_MAPS_CENTER = (-41.3, 32)
