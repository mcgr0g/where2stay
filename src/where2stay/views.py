from django.template import Context
from django.shortcuts import render, get_object_or_404

from forms import *


def home(request):
	context = Context({})
	return render(request, 'home.html', context)


def contact(request):
	if request.method == 'POST':
		form = ContactForm(request.POST)
		if form.is_valid():
			sender = form.cleaned_data['sender']
			subject = form.cleaned_data['subject']
			message = form.cleaned_data['message']
			message += "\nrequest from sender is %s" % sender
			from django.core.mail import mail_managers
			mail_managers(subject, message, fail_silently=False, connection=None, html_message=None)
	else:
		form = ContactForm()

	# context = Context()
	return render(request, "contact.html", {'form': form, })