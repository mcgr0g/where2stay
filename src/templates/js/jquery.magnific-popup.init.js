/**
 * Created with PyCharm.
 * User: vlad
 * Date: 30.08.13
 * Time: 7:13
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function() {

$('.lightbox').magnificPopup({
    type:'image',
    image: {
        titleSrc: function(item) {
            return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
        }
    }
//    gallery:{enabled:true},
//    callbacks: {
//        buildControls: function() {
//        this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
//       }
//    }
});

$('.gmap').magnificPopup({
    type: 'iframe',
    iframe: {
        markup: '<div class="mfp-iframe-scaler">'+
                    '<div class="mfp-close"></div>'+
                    '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button
        patterns: {
            gmaps: {
                index: '//maps.google.',
                src: '%id%&output=embed'
            }
        // you may add here more sources
        },
        srcAction: 'iframe_src' // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
    }
});

$('.panorama').magnificPopup({
    type: 'iframe',
    iframe: {
        markup: '<div class="mfp-iframe-scaler">'+
                    '<div class="mfp-close"></div>'+
                    '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button
        patterns: {
            panorama: {
                src: '{{ MEDIA_URL }}{{ int.photo }}'
            }
        // you may add here more sources
        },
        srcAction: 'iframe_src' // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
    }

});

});

